
// Playing Cards
// Devon Lozier



// Fork By: Devon Norman
/*=-=-=-  Stuff Added in this fork below!  -=-=-=
* REMOVED THE "CLASS" KEYWORD FROM BOTH ENUM'S DECLERATIONS AS IT PREVENT THEIR VALUES FROM BEING USED
* ADDED IN THE PRINTCARD METHOD
* ADDED IN THE HIGHCARD METHOD
* ADDED EXAMPLE PRINTS IN THE MAIN SECTION OF THE CODE, THESE CAN BE EDITED AND ALTERED TO ADD MORE CARDS, OR HAVE MORE COMPARISONS, OR TO REMOVE EITHER.
*/

#include <iostream>
#include <conio.h>

using namespace std;


//This Enum is used to store all possible rank values for the cards.
enum Rank
{
	TWO = 2,	//2
	THREE,	//3
	FOUR,	//4
	FIVE,	//5
	SIX,	//6
	SEVEN,	//7
	EIGHT,	//8
	NINE,	//9
	TEN,	//10
	JACK,	//11
	QUEEN,	//12
	KING,	//13
	ACE,	//14
	//JOKER should perhaps be included
};

//This enum is used to store all the possible Suits a card could have.
enum Suit
{
	//Goes from 0 - 3, 0 shall be the highest. 
	SPADES = 0, //Spades = 0
	HEARTS,		//Hearts = 1
	DIAMONDS,	//Diamonds = 2
	CLUBS		//Clubs = 3
};

//Creates a struct for a card object that stores it's rank and suit.
struct Card
{
	Rank rank; //Used to store rank in the struct
	Suit suit; //Used to store suit in the struct
};



//These folllowing functions are all created by Devon Norman for Part 2 of the assignment.

//This void is used to print out the information for a card.
void PrintCard(Card card)
{
	//The first word of the sentence structure that will be first printed out.
	std::cout << "The ";

	//This switch statement passes in the rank of the card instance to determine it's value to print to console.
	//TO CHECK THE CORRELATING VALUES IN THE RANK ENUM, CHECK THE COMMENTS BY EACH CARD RANK.
	switch (card.rank) 
	{

	//For card.rank = 2
	case 2:
		cout << "Two";
		break;

	//For card.rank = 3
	case 3:
		cout << "Three";
		break;

	//For card.rank = 4
	case 4:
		cout << "Four";
		break;

	//For card.rank = 5
	case 5:
		cout << "Five";
		break;

	//For card.rank = 6
	case 6:
		cout << "Six";
		break;

	//For card.rank = 7
	case 7:
		cout << "Seven";
		break;

	//For card.rank = 8
	case 8:
		cout << "Eight";
		break;

	//For card.rank = 9
	case 9:
		cout << "Nine";
		break;

	//For card.rank = 10
	case 10:
		cout << "Ten";
		break;

	//For card.rank = 11
	case 11:
		cout << "Jack";
		break;

	//For card.rank = 12
	case 12:
		cout << "Queen";
		break;

	//For card.rank = 13
	case 13:
		cout << "King";
		break;

	//For card.rank = 14
	case 14:
		cout << "Ace";
		break;
	}

	//The second cout statement is used to print out the word "Of" for the third word in the sentence.
	std::cout << " of ";


	/*This switch case is used to define the value of suits when ranked up against each other. This will
	* be used by the highcard to determine who will win in a tiebreaker. 
	* NOTE: THE RANKING OF THE SUITS ARE BASED OFF THEIR RANK ORDER IN POKER*/
	switch (card.suit)
	{
	//For card.suit of Spades			(NOTE: endl; << "\n" is added for BUILT-IN formatting.
	case 0:
		cout << "Spades" << endl << "\n";
		break;

	//For card.suit of Hearts
	case 1:
		cout << "Hearts" << endl << "\n";
		break;

	//For card.suit of Diamonds
	case 2:
		cout << "Diamonds" << endl << "\n";
		break;

	//For card.suit of Clubs
	case 3:
		cout << "Clubs" << endl << "\n";
		break;
	}


}

Card HighCard(Card card1, Card card2)
{
	/*This if statement is used to compare card1and card2 to each other's ranks. It will catch which
	* card has the higher rank value, and will then print out that card. A tie-breaker will run if needed.*/

	if (card1.rank > card2.rank)
	{
		return card1;
	}
	else if (card1.rank < card2.rank)
	{
		return card2;
	}
	//This else statement will run if the ranks have a tie, meaning ranks will be the tiebreaker.
	else 
	{
		//Runs if card1.suit is higher than card2.suit
		if (card1.suit > card2.suit)
		{
			return card1;
		}
		//Runs if card2.suit is higher than card1.suit (ALSO A LAST RESORT TO ENSURE A WINNER IS CHOSEN).
		else
		{
			return card2;
		}
	}
	
	//THIS FUNCTION BY ITSELF WILL NOT PRINT OUT INTO CONSOLE, NOR SHOULD IT.
}


int main()
{
	//Example Card Instances and defining

	//Used for formatting to give this section a nicer heading.
	cout << "-=-=-= PRINTED CARDS =-=-=-" << endl;

	//Defining & Printing Card A
	Card cardA;
	cardA.rank = SEVEN;
	cardA.suit = CLUBS;
	cout << "Card A: "; //Used to show what card the data is relating to.
	PrintCard(cardA); //Will run the values through the printCard method to finalize the card.

	//Defining & Printing Card B
	Card cardB;
	cardB.rank = FOUR;
	cardB.suit = SPADES;
	cout << "Card B: "; //Used to show what card the data is relating to.
	PrintCard(cardB); //Will run the values through the printCard method to finalize the card.
	
	//Defining & Printing Card C
	Card cardC;
	cardC.rank = KING;
	cardC.suit = DIAMONDS;
	cout << "Card C: "; //Used to show what card the data is relating to.
	PrintCard(cardC); //Will run the values through the printCard method to finalize the card.

	//Defining & Printing Card D
	Card cardD;
	cardD.rank = KING;
	cardD.suit = HEARTS;
	cout << "Card D: "; //Used to show what card the data is relating to.
	PrintCard(cardD); //Will run the values through the printCard method to finalize the card.

	//=============================================================================================

	// NOW LETS DO SOME TEST CALCULATIONS USING THE HIGHCARD METHOD...

	//Used to set up the highest ranking card formatting. Will display the winner.
	cout << "---HIGHEST RANKING CARD---" << endl;

	/*SOME EXAMPLES BELOW, EACH ONE WILL SHOW THE WINNER BETWEEN TWO CARDS WITH CONSIDERATION OF RANK AND SUIT.
	* FEEL FREE TO COMMENT OUT SOME OF THESE PRE-SET EXAMPLES AND CREATE NEW ONES USING CARDS A, B, C, D.
	* 
	* NOTE: HIGHCARD IS NOT THE ONE PRINTING OUT THE DATA, THAT IS 'PRINTCARD' PRINTING THE RESULTS OF 'HIGHCARD'. JUST TO CLEAR UP CONFUSION*/

	//Card A vs. Card B example.
	cout << "\n" << "-=-= Card A vs.Card B EXAMPLE =-=- " << endl;
	PrintCard(HighCard(cardA, cardB)); //Compares the two cards against each other to find the higher one. Prints the higher one.

	//Card C vs. Card D Example.
	cout << "\n" << "-=-= Card C vs.Card D EXAMPLE =-=- " << endl;
	PrintCard(HighCard(cardC, cardD)); //Compares the two cards against each other to find the higher one.

	//===============================================================================================

	//Card A vs. Card C Example.
	cout << "\n" << "-=-= Card A vs.Card C EXAMPLE =-=- " << endl;
	PrintCard(HighCard(cardA, cardC)); //Compares the two cards against each other to find the higher one.

	//Card B vs. Card D Example.
	cout << "\n" << "-=-= Card B vs.Card D EXAMPLE =-=- " << endl;
	PrintCard(HighCard(cardB, cardD)); //Compares the two cards against each other to find the higher one.

	//================================================================================================

	//Card A vs. Card D Example.
	cout << "\n" << "-=-= Card A vs.Card D EXAMPLE =-=- " << endl;
	PrintCard(HighCard(cardA, cardD)); //Compares the two cards against each other to find the higher one.

	//Card B vs. Card C Example.
	cout << "\n" << "-=-= Card B vs.Card C EXAMPLE =-=- " << endl;
	PrintCard(HighCard(cardB, cardC)); //Compares the two cards against each other to find the higher one.


	(void)_getch();
	return 0;
}
